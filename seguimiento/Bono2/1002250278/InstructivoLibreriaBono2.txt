Asegurarse de correr colab en version 'Python 3.10.12'

Instalación:
!pip install Bono2SDGC
import Bono2SDGC as bn

Requerimiento:
Tener el .csv a la mano :)

Instrucciones de uso:
La libreria tiene 2 clases:

1) Ajuste:

   La clase recibe 2 argumentos
   1) d : La direccion del .csv
   2) s : el valor de la desviación estándar

   La clase ajuste tiene 4 métodos
   1) Dataframe: es un método interno que extrae info del .csv
   2) Kernel: método interno que actualiza los valores necesarios (Gaussianas, etc)
   3) Graph:  Grafica el resultado de los Kernel
   4) Derivada: Deriva y grafica la funcion del Kernel

2) Integrales

   La clase recibe 4 argumentos
   1) a: Inicio de intervalo de evaluacion
   2) b: Final de intervalo de evaluacion
   3) n: Número de generaciones en el intervalo de evaluación
   4) f: Función

   La clase tiene 1 método
   1) Integrate: integra la función en el intervalo proporcionado


Para correr lo necesario se recomienda:

!pip install Bono2SDGC

import Bono2SDGC as bn

d="SU_DIRECCION_DE_CSV"
sigma= 9                                #DESVIACION ESTANDAR

V=bn.Ajuste(d,sigma)		
V.Graph()				# Da gráfica de método de Kernel
V.Derivada()			 	# Da gráfica de derivada

#######################################
a = 0
b = 1
n = 100000
f = lambda x: x**2			# Su función de agrado
P=bn.Integrales(a, b, n, f)
P.Integrate()
