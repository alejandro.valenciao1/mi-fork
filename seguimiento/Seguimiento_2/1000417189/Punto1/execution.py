from mcIntegral import mcInt
import sympy as smp
import numpy as np

if __name__=="__main__":
    print("Procesando...\n")

    f=lambda x: np.tanh(x/7)#x**2*np.cos(x)
    a=0
    b=1#np.pi
    N=100000

    prob1=mcInt(f,a,b,N)
    print(f"Montecarlo para {N} iteraciones:",prob1.mcAreas())
    print("Analítica:", prob1.analitica()[0])
    prob1.plot()

    