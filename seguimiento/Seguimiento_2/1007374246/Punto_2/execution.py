from moleculas import particula

if __name__ == "__main__":
    print("running")
    L = 1 #Longitud de la caja
    n = 2 #npumero cuantico
    n2 = 200 #Parametro para el plot de la función de onda teórica
    muestras = 15000 #Numero de muestras para el metodo de montecarlo

    box = particula(L,n,muestras,n2)
    box.run()
    print("terminado")