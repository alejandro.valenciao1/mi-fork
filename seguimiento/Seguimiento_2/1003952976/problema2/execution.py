from problema_planteado import Ideal_Gas

if __name__=="__main__":

    # Parámetros del sistema
    num_particles = 10
    num_steps = 50
    temperature = 1.0  # Puedes ajustar la temperatura
    box_size = 10.0
    particle_mass = 1.0

    # Simulación
    gas = Ideal_Gas(num_particles, num_steps, temperature, box_size, particle_mass)
    gas.plot()
    gas.animated() #ojo, esta simulación se demora algo de tiempo para arrojar la imagen .gif
    gas.run()

