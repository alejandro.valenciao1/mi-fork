import numpy as np
import random
import matplotlib.pyplot as plt
import copy
import sys

class Ising:

    """
    Esta clase realiza una simulacion del modelo Ising.
    J: Interaccion (J>0 ferromagnetico, J<0 antiferromangetico, J=0 noninteracting)
    numIter: numero de iteraciones que se quieren realizar.
    beta: 1/T donde T es temperatura
    tamanoArray: Tamaño del array que describe el sistema.
    """

    def __init__(self, J, numIter, beta, tamanoArray):
        self.J = J
        self.t = int(numIter)
        self.beta = beta
        self.N = int(tamanoArray)

    def errores(self):
        try:
            self.J / 2
        except:
            print('Ingrese un valor de J válido.')
            sys.exit(1)

        try:
            1 / (abs(self.t) + self.t)
        except:
            print('Ingrese un entero mayor a cero para el numero de iteraciones.')
            sys.exit(1)

        try:
            1 / (abs(self.N) + self.N)
        except:
            print('Ingrese un entero mayor a cero para el tamaño del arreglo.')
            sys.exit(1)

        try:
            self.beta / 2
        except:
            print('Ingrese un valor de beta válido.')
            sys.exit(1)

        
            

    def array(self):

        #Definimos el array que va a describir el sistema. El 95% de este va a ser spin down.
        init_random = np.random.random((self.N, self.N))
        spins = np.zeros((self.N, self.N))
        spins[init_random>=0.95] = 1
        spins[init_random<0.95] = -1
        return spins

    def energia(self, x, a, b, spins):

        #Funcion que calcula la energia de uno de los espines en el arreglo.
        suma = 0
        if a > 0:
            suma += spins[a-1, b]
        if b > 0:
            suma += spins[a, b-1]
        if a < self.N-1:
            suma += spins[a+1, b]
        if b < self.N-1:
            suma += spins[a, b+1]
        return - x * suma
    
    def magnetizacion(self, arr):

        #Magnetizacion del sistema
        return np.sum(arr)
    
    def MC(self):
        
        self.errores()

        #Desarrollo del sistema con Monte Carlo
        spins = self.array()
        spins_f = copy.deepcopy(spins)
        mag = []

        for _ in range(self.t):
            a = random.randint(0, self.N-1)
            b = random.randint(0, self.N-1)
            spin = spins_f[a,b]
                
            ener_mu = self.energia(spin, a, b, spins_f)
            ener_nu = self.energia(-spin, a, b, spins_f)
            P = np.exp(- self.beta * self.J * (ener_nu - ener_mu))
                
            if ener_mu < ener_nu or random.random() < P:
                spin *= -1
            spins_f[a, b] = spin
            mag.append(self.magnetizacion(spins_f))
        return spins, spins_f, mag
    
    def figMagnetizacion(self):

        #Graficamos la evolucion de la magnetizacion en cada uno de los pasos del modelo Icing.
        resultado  = self.MC()
        t = range(self.t)
        
        plt.scatter(t, resultado[2])
        plt.title('Evolucion de la magnetizacion del sistema')
        plt.ylabel('Magnetizacion')
        plt.xlabel('tiempo')
        plt.savefig('Magnetizacion.png')
    
    def plots(self):

        #Graficamos el antes y el después de usar el modelo
        resultado = self.MC()
        plot_antes = plt.subplot2grid((1, 2), (0, 0))
        plot_despues = plt.subplot2grid((1, 2), (0, 1))

        plot_antes.imshow(resultado[0])
        plot_antes.set_title('Antes de Ising')

        plot_despues.imshow(resultado[1])
        plot_despues.set_title('Despues de Ising')
        plt.savefig('Antes_vs_despues.png')
