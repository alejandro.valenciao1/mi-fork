-----------------------------------------------------------------------------------------

Para instalar la librería:

pip install Libkernelsfabian


-----------------------------------------------------------------------------------------

from kernelsfabian import Kernel # importar la librería
import pandas as pd # importar librería auxiliar 

sigma=1

data=pd.read_csv("datos_covid.csv") # cargar el archivo de datos de covid 

objeto=Kernel(data,sigma)  # declarar objeto de la clase Kernel

objeto.plotepanechnikov() # gráficas
objeto.plotgaussian()
objeto.plottricube()
objeto.plotcomparation()
