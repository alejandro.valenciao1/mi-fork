from Ising import Ising

if __name__ == "__main__":

    N=10000 #Número de iteraciones MonteCarlo
    L=5 #tamaño cuadrícula
    N_t=5 #cantidad de temperaturas
    ti=0.1 #Temperatura inicial
    tf=7 #Temperatura final
    ising=Ising(L,N,ti,tf,N_t)
    ising.magnetic() 
    ising.calor() 