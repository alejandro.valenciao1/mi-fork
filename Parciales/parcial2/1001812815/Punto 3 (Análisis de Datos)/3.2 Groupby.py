import pandas as pd

# Leer los datos desde el archivo
df = pd.read_csv('datos_alcohol.txt')

# Agrupar por continente
grouped_by_continent = df.groupby('continente')

# 1. ¿Qué continente toma más cerveza en promedio?
media_cerveza_por_continente = grouped_by_continent['porciones_cerveza'].mean()
continente_max_cerveza = media_cerveza_por_continente.idxmax()
print(f"El continente que toma más cerveza en promedio es: {continente_max_cerveza}")

# 2. Analizar la columna de vino e imprimir un análisis básico estadístico del consumo.
analisis_vino = df['porciones_vino'].describe()
print("\nAnálisis estadístico del consumo de vino:")
print(analisis_vino)

# 3. Para cada continente imprime los valores medio, mínimo y máximo para el consumo de licor.
estadisticas_licor_por_continente = grouped_by_continent['porciones_licor'].agg(['mean', 'min', 'max'])
print("\nEstadísticas para el consumo de licor por continente:")
print(estadisticas_licor_por_continente)

# 4. Para cada continente imprime el consumo medio de alcohol para todas las columnas.
consumo_medio_por_continente = grouped_by_continent.mean()
print("\nConsumo medio de alcohol para todas las columnas por continente:")
print(consumo_medio_por_continente)

