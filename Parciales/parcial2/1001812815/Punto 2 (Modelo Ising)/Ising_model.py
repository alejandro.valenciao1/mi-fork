import numpy as np
import matplotlib.pyplot as plt

class IsingModel2D:
    def __init__(self, L_values=[2, 4, 6], T_min=1, T_max=5.0, num_T=50, k_B=1.0, J=1.0):
        self.L_values = L_values
        self.T_list = np.linspace(T_min, T_max, num_T)
        self.k_B = k_B
        self.J = J

    def initialize_spins(self, L):
        """Inicializa la red de espines."""
        return np.random.choice([-1, 1], size=(L, L))

    def calculate_energy(self, spins):
        """Calcula la energía de la configuración actual."""
        return -self.J * np.sum(spins * (np.roll(spins, 1, axis=0) + np.roll(spins, 1, axis=1)))

    def calculate_specific_heat(self, energies, T, L):
        """Calcula la capacidad calorífica."""
        beta = 1.0 / (self.k_B * T)
        E_mean, E2_mean = np.mean(energies), np.mean(energies**2)
        return beta**2 * (E2_mean - E_mean**2) / L**2

    def calculate_average_energy(self, energies):
        """Calcula la energía promedio."""
        return np.mean(energies)

    def calculate_magnetization(self, spins):
        """Calcula la magnetización promedio."""
        return np.mean(spins)

    def make_change(self, spins, T, L):
        """Realiza un cambio en la configuración de espines."""
        i, j = np.random.randint(0, L, size=2)
        E_old = self.calculate_energy(spins)
        spins[i, j] *= -1
        E_new = self.calculate_energy(spins)

        if E_new < E_old or np.random.rand() < np.exp(-(E_new - E_old) / (self.k_B * T)):
            return spins, E_new
        else:
            spins[i, j] *= -1
            return spins, E_old

    def simulate(self, num_steps):
        C_dict = {}
        magnetization_dict = {}

        for L in self.L_values:
            C_list = []
            magnetization_list = []

            for T in self.T_list:
                spins = self.initialize_spins(L)
                energies = np.zeros(num_steps)

                for step in range(num_steps):
                    spins, E = self.make_change(spins, T, L)
                    energies[step] = E

                C_list.append(self.calculate_specific_heat(energies, T, L))
                magnetization_list.append(self.calculate_magnetization(spins))

            C_dict[L] = C_list
            magnetization_dict[L] = magnetization_list

        return C_dict, magnetization_dict

    def plot_combined(self, C_dict, magnetization_dict):
        for L in self.L_values:
            plt.scatter(self.T_list, C_dict[L], marker='o', label=f'L={L} - Calor Específico')
        
        plt.xlabel('Temperatura')
        plt.ylabel('Calor Específico')
        plt.title('Calor Específico vs Temperatura para una Red 2D')
        plt.savefig('Calor Específico vs Temperatura para una Red 2D.png')
        plt.legend()
        plt.show()

        for L in self.L_values:
            plt.scatter(self.T_list, np.array(magnetization_dict[L]), marker='o', label=f'L={L} - Magnetización')

        plt.xlabel('Temperatura')
        plt.ylabel('Magnetización')
        plt.title('Magnetización vs Temperatura para una Red 2D')
        plt.legend()
        plt.savefig('Magnetización vs Temperatura para una Red 2D.png')
        plt.show()

