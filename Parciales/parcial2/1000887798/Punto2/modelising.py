import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm  
from scipy.optimize import curve_fit


class isingModel:

    """
    Clase que contiene los métodos para simular el modelo de Ising en 2D

    """

    def __init__(self,N,n,paso,a,b):

        """
        Constructor de la clase 
        """

        self.N = N #Tamaño de la rejilla de espines
        self.nt = n
        self.paso = paso
        self.a  = a #TInicial
        self.b = b #TFinal
    
    def initialConfig(self):
            
            """
            Método que inicializa la configuración de espines
            """
    
            state = 2*np.random.randint(2, size=(self.N,self.N))-1
            return state
    
    

